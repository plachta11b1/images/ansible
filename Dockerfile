FROM ubuntu AS base

ARG IMAGE_BUILD_VERSION=0.0.0

ENV DEBIAN_FRONTEND=noninteractive
ENV IMAGE_BUILD_VERSION $IMAGE_BUILD_VERSION

RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:ansible/ansible
RUN apt-get update
RUN apt-get install -y --no-install-recommends net-tools
RUN apt-get install -y --no-install-recommends dnsutils
RUN apt-get install -y --no-install-recommends ansible
RUN apt-get install -y --no-install-recommends openssh-client



FROM base AS docker

RUN ansible-galaxy install konstruktoid.docker_rootless
RUN ansible-galaxy collection install community.docker
